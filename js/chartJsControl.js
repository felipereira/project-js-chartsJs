var dataPoints = [];
var chartBarCollor = [];
var borderChart = [];
var identificadores = ["sala", "fin", "eng", "fat", "tec"];
var rgba;
var border;

$.getJSON("http://10.40.0.237:8080/Zatix/temperatura/listarSensores", function (data) {
    $.each(data.cliente, function (key, item) {
        dataPoints.push
            (
            parseFloat(item.value)
            );
    });
    //change row of collor
    for (let _i = 0; _i < dataPoints.length; _i++) {

        if (dataPoints[_i] > 23.0) {
            id = document.getElementById(identificadores[_i]);
            id.className = (id.className == 'table-danger') ? identificadores[_i] : 'table-danger';
            $("#" + identificadores[_i]).append("<td>" + dataPoints[_i] + " °C </td>");

            rgba = 'rgba(255,0,0, 0.2)';
            border = 'rgba(255,0,0, 0.4)';
            chartBarCollor.push(rgba);
            borderChart.push(border);

        } else
            if (dataPoints[_i] < 20.0) {
                var id = document.getElementById(identificadores[_i]);
                id.className = (id.className == 'table-primary') ? identificadores[_i] : 'table-primary';
                $("#" + identificadores[_i]).append("<td>" + dataPoints[_i] + " °C </td>");

                rgba = 'rgba(0,0,255, 0.2)';
                border = 'rgba(0,0,255, 0.4)';
                chartBarCollor.push(rgba);
                borderChart.push(border);
            }
            else
                if (dataPoints[_i] >= 20.0 && dataPoints[_i] <= 23.0) {

                    var id = document.getElementById(identificadores[_i]);

                    id.className = (id.className == 'table-light') ? identificadores[_i] : 'table-light';
                    $("#" + identificadores[_i]).append("<td>" + dataPoints[_i] + " °C </td>");

                    rgba = 'rgba(0,0,255, 0.0)';
                    border = 'rgba(0,0,0, 0.4)';
                    chartBarCollor.push(rgba);
                    borderChart.push(border);
                }
    }

    new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
            labels: ["Sala 08", "Financeiro", "Engenharia", "Faturamento", "Tecnologia"],
            datasets: [{
                backgroundColor: chartBarCollor,
                borderColor: borderChart,
                borderWidth: 1,
                label: "Temperatura",
                data: dataPoints
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Gráfico de Temperatura'
            },
            legend: {
                display: false,
                labels: {
                    fontColor: 'rgb(0,0,0)'
                }
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        drawBorder: false,
                        color: ['rgba(0,0,255, 0.1)', 'rgba(0,0,255, 0.1)', 'red', 'rgba(0,0,255, 0.1)', 'rgba(0,0,255, 0.1)', 'blue', 'rgba(0,0,255, 0.1)']
                    },
                    ticks: {
                        beginAtZero: true,
                        min: 18,
                        max: 25,
                        stepSize: 1
                        
                    }
                }]
            }
        }
    });

    $('#time').append('<b><font size="2">Última atualização: ', dateNow().fontsize(2) + '</font>    </b>');
});

function dateNow() {

    var today = new Date();
    var date = today.toLocaleDateString();
    var hour = today.toLocaleTimeString();

    return (hour + ' ' + date);
}

setTimeout(function () {

    location.reload();

}, 120000);